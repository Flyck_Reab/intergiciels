package linda.server_storable;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Shared memory implementation of Linda.
 */
public class CentralizedLinda implements Linda, Serializable {

    private static final long serialVersionUID = 7644326164711247081L;

    /**
     * Pile of tulpe present in the server
     */
    private List<Tuple> TSpace;
    /**
     * Mutex for @TSpace
     */
    ReentrantLock tSpaceLock;
    /**
     * Name of the file containing server data
     */
    private final String T_SPACE_STORAGE_LOCATION = "t_space.data";

    /**
     * List of templates waiting to get corresponding Tuple
     */
    private List<TemplateWaiting> templateWaitingList;
    /**
     * Mutex for @templateWairingList
     */
    ReentrantLock templateWaitingListLock;


    /**
     * Constructor
     * If a file corresponding to T_SPACE_STORAGE_LOCATION exists, it will try to load data from it.     *
     */
    public CentralizedLinda() {
        try {
            TSpace = loadTSpace(T_SPACE_STORAGE_LOCATION);
        } catch (IOException e) {
            System.err.println("Error while closing storage file : " + T_SPACE_STORAGE_LOCATION);
        }
        if (TSpace == null) {
            TSpace = new ArrayList<>();
        }
        tSpaceLock = new ReentrantLock();

        templateWaitingList = new ArrayList<>();
        templateWaitingListLock = new ReentrantLock();
    }

    @Override
    public void write(Tuple t) {
        tSpaceLock.lock();
        this.TSpace.add(t);
        try {
            this.storeTSpace(T_SPACE_STORAGE_LOCATION);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tSpaceLock.unlock();

        //Wake up next waiter
        notifyNextTemplate(t);
    }

    /**
     * Find every template corresponding to the given tuple
     * Notify each of them to restart a search
     */
    private void notifyNextTemplate(Tuple tuple) {
        List<TemplateWaiting> templatesToNotify = new ArrayList<>();
        //Find the templates to notify
        templateWaitingListLock.lock();
        for (TemplateWaiting templateWaiting : this.templateWaitingList) {
            if (tuple.matches(templateWaiting.template)) {
                templatesToNotify.add(templateWaiting);
                if (templateWaiting.mode == eventMode.TAKE) {
                    break;
                }
            }
        }
        templateWaitingListLock.unlock();

        //Notify the templates
        for (TemplateWaiting templateWaitingToNotify : templatesToNotify) {
            if (templateWaitingToNotify.callback == null) {
                synchronized (templateWaitingToNotify.template) {
                    templateWaitingToNotify.template.notify();
                }
            } else {
                notifyCallback(templateWaitingToNotify);
            }
        }
    }

    /**
     * Try to take tuple corresponding to the template of the callback and call it if fount.
     */
    private void notifyCallback(TemplateWaiting templateWaitingToNotify) {
        Tuple tupleToSend = null;
        switch (templateWaitingToNotify.mode) {
            case READ:
                tupleToSend = tryRead(templateWaitingToNotify.template);
                break;
            case TAKE:
                tupleToSend = tryTake(templateWaitingToNotify.template);
                break;
        }
        if (tupleToSend != null) {
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaitingToNotify);
            templateWaitingListLock.unlock();
            templateWaitingToNotify.callback.call(tupleToSend);
        }
    }

    @Override
    public Tuple take(Tuple template) {
        Tuple tuple = tryTake(template);
        while (tuple == null) {
            TemplateWaiting templateWaiting = new TemplateWaiting(template, eventMode.TAKE);
            synchronized (template) {
                try {
                    templateWaitingListLock.lock();
                    templateWaitingList.add(templateWaiting);
                    templateWaitingListLock.unlock();
                    template.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tuple = tryTake(template);
            }
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaiting);
            templateWaitingListLock.unlock();
        }
        return tuple;
    }

    @Override
    public Tuple read(Tuple template) {
        Tuple tuple = tryRead(template);
        while (tuple == null) {
            TemplateWaiting templateWaiting = new TemplateWaiting(template, eventMode.READ);
            synchronized (template) {
                try {
                    templateWaitingListLock.lock();
                    templateWaitingList.add(templateWaiting);
                    templateWaitingListLock.unlock();
                    template.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tuple = tryRead(template);
            }
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaiting);
            templateWaitingListLock.unlock();
        }
        return tuple;
    }

    @Override
    public Tuple tryTake(Tuple template) {
        Tuple lastCorrespondingTuple = null;
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                lastCorrespondingTuple = tuple;
            }
        }
        TSpace.remove(lastCorrespondingTuple);
        try {
            this.storeTSpace(T_SPACE_STORAGE_LOCATION);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tSpaceLock.unlock();
        return lastCorrespondingTuple;
    }

    @Override
    public Tuple tryRead(Tuple template) {
        Tuple lastCorrespondingTuple = null;
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                lastCorrespondingTuple = tuple;
            }
        }
        tSpaceLock.unlock();
        return lastCorrespondingTuple;
    }

    @Override
    synchronized public Collection<Tuple> takeAll(Tuple template) {
        Collection<Tuple> tuples = new ArrayList<>();
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                tuples.add(tuple);
            }
        }
        TSpace.removeAll(tuples);
        try {
            this.storeTSpace(T_SPACE_STORAGE_LOCATION);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tSpaceLock.unlock();
        return tuples;
    }

    @Override
    synchronized public Collection<Tuple> readAll(Tuple template) {
        Collection<Tuple> tuples = new ArrayList<>();
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                tuples.add(tuple);
            }
        }
        tSpaceLock.unlock();
        return tuples;
    }

    @Override
    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        Tuple tuple = null;
        if (timing == eventTiming.IMMEDIATE) {
            switch (mode) {
                case READ:
                    tuple = tryRead(template);
                    break;
                case TAKE:
                    tuple = tryTake(template);
                    break;
            }
        }
        if (tuple == null) {
            TemplateWaiting templateWaiting = new TemplateWaiting(template, mode, callback);
            templateWaitingListLock.lock();
            templateWaitingList.add(templateWaiting);
            templateWaitingListLock.unlock();
        } else {
            callback.call(tuple);
        }
    }

    @Override
    public void debug(String prefix) {
        ArrayList<Tuple> templateWaitingListTemp = new ArrayList<>();
        templateWaitingListLock.lock();
        for (TemplateWaiting templateWaiting : templateWaitingList) {
            templateWaitingListTemp.add(templateWaiting.template);
        }
        templateWaitingListLock.unlock();
        tSpaceLock.lock();
        System.out.println(prefix + " TSpace : " + TSpace);
        tSpaceLock.unlock();
        System.out.println(prefix + " TemplateWaiting : " + templateWaitingListTemp);
    }

    /**
     * A class to represent template waiting to be notified/called
     */
    protected class TemplateWaiting implements Serializable {

        private static final long serialVersionUID = 7066143115091934342L;

        protected eventMode mode;
        protected Tuple template;
        protected Callback callback;

        /**
         * Constructor
         * null Callback means that it is not a callback but a regular take/read
         */
        TemplateWaiting(Tuple template, eventMode mode, Callback callback) {
            this.template = template;
            this.mode = mode;
            this.callback = callback;
        }

        /**
         * Constructor
         * Callback not specified means that it is not a callback
         */
        TemplateWaiting(Tuple template, eventMode mode) {
            this.template = template;
            this.mode = mode;
            this.callback = null;
        }
    }

    /**
     * Store current TSpace in a data file
     *
     * @param location the location of the file to store
     * @throws IOException If an error occurs while saving data
     */
    private void storeTSpace(String location) throws IOException {
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(location, false);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(TSpace);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (oos != null) {
                oos.close();
            }
        }
    }

    /**
     * Load list of tuple from a file data at locataion
     *
     * @param location the data filename
     * @return a list of tuple loaded from data file
     * @throws IOException If an error occurs while loading data
     */
    private List<Tuple> loadTSpace(String location) throws IOException {
        ObjectInputStream ois = null;
        List<Tuple> readCase = null;
        try {
            FileInputStream streamIn = new FileInputStream(location);
            ois = new ObjectInputStream(streamIn);
            readCase = (List<Tuple>) ois.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("No data file to load at : " + location);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                ois.close();
            }
        }
        return readCase;
    }
}
