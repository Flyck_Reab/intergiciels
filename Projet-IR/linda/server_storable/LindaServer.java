package linda.server_storable;

import linda.server.LindaRemote;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class LindaServer {
    public static void main(String args[]) throws Exception {

        //  Création du serveur de noms
        try {
            LocateRegistry.createRegistry(1099);
        } catch (java.rmi.server.ExportException e) {
            System.out.println("A registry is already running, proceeding...");
        }

        //  Création de l'objet Linda,
        //  et enregistrement de linda dans le serveur de nom
        LindaRemote linda = new LindaRemoteImpl();
        Registry dns = LocateRegistry.getRegistry("localhost", 1099);
        dns.bind("MonLinda", linda);

        // Service prêt : attente d'appels
        System.out.println("Le systeme est pret.");
    }
}
