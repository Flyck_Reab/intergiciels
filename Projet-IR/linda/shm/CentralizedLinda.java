package linda.shm;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of Linda.
 */
public class CentralizedLinda implements Linda {

    /**
     * Pile of Tulpes present in the server
     */
    List<Tuple> TSpace;
    /** Mutex for @TSpace */
    ReentrantLock tSpaceLock;

    /** List of templates waiting to get corresponding Tuple*/
    List<TemplateWaiting> templateWaitingList;
    /** Mutex for @templateWaitingList */
    ReentrantLock templateWaitingListLock;


    public CentralizedLinda() {
        TSpace = new ArrayList<>();
        tSpaceLock = new ReentrantLock();

        templateWaitingList = new ArrayList<>();
        templateWaitingListLock  = new ReentrantLock();
    }

    @Override
    public void write(Tuple t) {
        tSpaceLock.lock();
        this.TSpace.add(t);
        tSpaceLock.unlock();

        //Wake up next waiter
        notifyNextTemplate(t);
    }

    /**
     * Find every template corresponding to the given tuple
     * Notify each of them to restart a search
     */
    private void notifyNextTemplate(Tuple tuple) {
        List<TemplateWaiting> templatesToNotify = new ArrayList<>();
        //Find the templates to notify
        templateWaitingListLock.lock();
        for (TemplateWaiting templateWaiting : this.templateWaitingList) {
            if (tuple.matches(templateWaiting.template)) {
                templatesToNotify.add(templateWaiting);
                if (templateWaiting.mode == eventMode.TAKE) {
                    break;
                }
            }
        }
        templateWaitingListLock.unlock();

        //Notify the templates
        for (TemplateWaiting templateWaitingToNotify : templatesToNotify) {
            if (templateWaitingToNotify.callback == null) {
                synchronized (templateWaitingToNotify.template) {
                    templateWaitingToNotify.template.notify();
                }
            } else {
                notifyCallback(templateWaitingToNotify);
            }
        }
    }

    /**
     * Try to take tuple corresponding to the template of the callback and call it if fount.
     */
    private void notifyCallback(TemplateWaiting templateWaitingToNotify) {
        Tuple tupleToSend = null;
        switch (templateWaitingToNotify.mode) {
            case READ:
                tupleToSend = tryRead(templateWaitingToNotify.template);
                break;
            case TAKE:
                tupleToSend = tryTake(templateWaitingToNotify.template);
                break;
        }
        if (tupleToSend != null) {
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaitingToNotify);
            templateWaitingListLock.unlock();
            templateWaitingToNotify.callback.call(tupleToSend);
        }
    }

    @Override
    public Tuple take(Tuple template) {
        Tuple tuple = tryTake(template);
        while (tuple == null) {
            TemplateWaiting templateWaiting = new TemplateWaiting(template, eventMode.TAKE);
            synchronized (template) {
                try {
                    templateWaitingListLock.lock();
                    templateWaitingList.add(templateWaiting);
                    templateWaitingListLock.unlock();
                    template.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tuple = tryTake(template);
            }
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaiting);
            templateWaitingListLock.unlock();
        }
        return tuple;
    }

    @Override
    public Tuple read(Tuple template) {
        Tuple tuple = tryRead(template);
        while (tuple == null) {
            TemplateWaiting templateWaiting = new TemplateWaiting(template, eventMode.READ);
            synchronized (template) {
                try {
                    templateWaitingListLock.lock();
                    templateWaitingList.add(templateWaiting);
                    templateWaitingListLock.unlock();
                    template.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tuple = tryRead(template);
            }
            templateWaitingListLock.lock();
            templateWaitingList.remove(templateWaiting);
            templateWaitingListLock.unlock();
        }
        return tuple;
    }

    @Override
    public Tuple tryTake(Tuple template) {
        Tuple lastCorrespondingTuple = null;
        tSpaceLock.lock();
        for (Tuple tuple: TSpace) {
            if (tuple.matches(template)) {
                lastCorrespondingTuple = tuple;
            }
        }
        TSpace.remove(lastCorrespondingTuple);
        tSpaceLock.unlock();
        return lastCorrespondingTuple;
    }

    @Override
    public Tuple tryRead(Tuple template) {
        Tuple lastCorrespondingTuple = null;
        tSpaceLock.lock();
        for (Tuple tuple: TSpace) {
            if (tuple.matches(template)) {
                lastCorrespondingTuple = tuple;
            }
        }
        tSpaceLock.unlock();
        return lastCorrespondingTuple;
    }

    @Override
    synchronized public Collection<Tuple> takeAll(Tuple template) {
        Collection<Tuple> tuples = new ArrayList<>();
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                tuples.add(tuple);
            }
        }
        TSpace.removeAll(tuples);
        tSpaceLock.unlock();
        return tuples;
    }

    @Override
    synchronized public Collection<Tuple> readAll(Tuple template) {
        Collection<Tuple> tuples = new ArrayList<>();
        tSpaceLock.lock();
        for (Tuple tuple : TSpace) {
            if (tuple.matches(template)) {
                tuples.add(tuple);
            }
        }
        tSpaceLock.unlock();
        return tuples;
    }

    @Override
    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        Tuple tuple = null;
        if (timing == eventTiming.IMMEDIATE) {
            switch (mode){
                case READ:
                    tuple = tryRead(template);
                    break;
                case TAKE:
                    tuple = tryTake(template);
                    break;
            }
        }
        if (tuple == null){
            TemplateWaiting templateWaiting = new TemplateWaiting(template, mode, callback);
            templateWaitingListLock.lock();
            templateWaitingList.add(templateWaiting);
            templateWaitingListLock.unlock();
        }else{
            callback.call(tuple);
        }
    }

    @Override
    public void debug(String prefix) {
        ArrayList<Tuple> templateWaitingListTemp = new ArrayList<>();
        templateWaitingListLock.lock();
        for (TemplateWaiting templateWaiting : templateWaitingList) {
            templateWaitingListTemp.add(templateWaiting.template);
        }
        templateWaitingListLock.unlock();
        tSpaceLock.lock();
        System.out.println(prefix + " TSpace : " + TSpace);
        tSpaceLock.unlock();
        System.out.println(prefix + " TemplateWaiting : " + templateWaitingListTemp);
    }

    /**
     * A class to represent template waiting to be notified/called
     */
    protected class TemplateWaiting {
        protected eventMode mode;
        protected Tuple template;
        protected Callback callback;


        /**
         * Constructor
         * null Callback means that it is not a callback but a regular take/read
         */
        TemplateWaiting(Tuple template, eventMode mode, Callback callback) {
            this.template = template;
            this.mode = mode;
            this.callback = callback;
        }

        /**
         * Constructor
         * Callback not specified means that it is not a callback
         */
        TemplateWaiting(Tuple template, eventMode mode) {
            this.template = template;
            this.mode = mode;
            this.callback = null;
        }
    }
}
