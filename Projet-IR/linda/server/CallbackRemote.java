package linda.server;

import linda.Tuple;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Callback version remote when a tuple appears.
 */
public interface CallbackRemote extends Remote {

        /**
         * Callback when a tuple appears.
         * See Linda.eventRegister for details.
         *
         * @param t the new tuple
         */
        void call(Tuple t) throws RemoteException;
}
