package linda.server;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.shm.CentralizedLinda;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;

public class LindaRemoteImpl extends UnicastRemoteObject implements LindaRemote {

    Linda lindaProcessor;

    public LindaRemoteImpl() throws RemoteException {
        lindaProcessor = new CentralizedLinda();
    }

    @Override
    public void write(Tuple t) throws RemoteException {
        lindaProcessor.write(t);
    }

    @Override
    public Tuple take(Tuple template) throws RemoteException {
        return lindaProcessor.take(template);
    }

    @Override
    public Tuple read(Tuple template) throws RemoteException {
        return lindaProcessor.read(template);
    }

    @Override
    public Tuple tryTake(Tuple template) throws RemoteException {
        return lindaProcessor.tryTake(template);
    }

    @Override
    public Tuple tryRead(Tuple template) throws RemoteException {
        return lindaProcessor.tryRead(template);
    }

    @Override
    public Collection<Tuple> takeAll(Tuple template) throws RemoteException {
        return lindaProcessor.takeAll(template);
    }

    @Override
    public Collection<Tuple> readAll(Tuple template) throws RemoteException {
        return lindaProcessor.readAll(template);
    }

    @Override
    public void eventRegister(Linda.eventMode mode, Linda.eventTiming timing, Tuple template, CallbackRemote callbackRemote) throws RemoteException {
        Callback callback = new Callback() {
            @Override
            public void call(Tuple t) {
                try {
                    callbackRemote.call(t);
                } catch (RemoteException e) {
                    lindaProcessor.write(t);
                }
            }
        };
        lindaProcessor.eventRegister(mode, timing, template, callback);
    }

    @Override
    public void debug(String prefix) throws RemoteException {
        lindaProcessor.debug(prefix);
    }
}
