package linda.test.testRead;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;

/**
 * Test :
 * 5 write sur un seul thread
 * 5 read sur cinq thread différent
 *      Le premier read est bloquant tant que le dernier write n'est pas fait,
 *      mais il ne bloque pas les autre read/take des autres threads
 */
public class TestReadMultiWriteMono {


    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");


        ArrayList<Tuple> listeMotifRead = new ArrayList<>();
        Tuple t1 = new Tuple(Integer.class, String.class);
        Tuple t2 = new Tuple(Integer.class, Integer.class);
        Tuple t3 = new Tuple(String.class, String.class);
        Tuple t4 = new Tuple(Integer.class, Integer.class);
        Tuple t5 = new Tuple(String.class, Integer.class);
        listeMotifRead.add(t1);
        listeMotifRead.add(t2);
        listeMotifRead.add(t3);
        listeMotifRead.add(t4);
        listeMotifRead.add(t5);

        for (Tuple tuple : listeMotifRead){
            final Tuple motif = tuple;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Tuple res = linda.read(motif);
                    System.out.println("(1) Resultat:" + res);

                    linda.debug("(1)" + motif.toString());

                }
            }.start();
        }

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple("foo", "foo");
                System.out.println("(2) write: " + t1);
                linda.write(t1);

                Tuple t2 = new Tuple("foo", 1);
                System.out.println("(2) write: " + t2);
                linda.write(t2);

                Tuple t3 = new Tuple(1, 2);
                System.out.println("(2) write: " + t3);
                linda.write(t3);

                Tuple t4 = new Tuple(3, 5);
                System.out.println("(2) write: " + t4);
                linda.write(t4);

                Tuple t5 = new Tuple(1, "foo");
                System.out.println("(2) write: " + t5);
                linda.write(t5);

                linda.debug("(2)");

            }
        }.start();
    }
}
