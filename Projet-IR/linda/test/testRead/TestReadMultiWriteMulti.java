package linda.test.testRead;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;

/**
 * Test :
 * 5 write sur cinq threads différents
 * 5 read sur cinq threads différents
 */
public class TestReadMultiWriteMulti {


    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");


        ArrayList<Tuple> listeMotifRead = new ArrayList<>();
        Tuple tr1 = new Tuple(Integer.class, String.class);
        Tuple tr2 = new Tuple(Integer.class, Integer.class);
        Tuple tr3 = new Tuple(String.class, String.class);
        Tuple tr4 = new Tuple(Integer.class, Integer.class);
        Tuple tr5 = new Tuple(String.class, Integer.class);
        listeMotifRead.add(tr1);
        listeMotifRead.add(tr2);
        listeMotifRead.add(tr3);
        listeMotifRead.add(tr4);
        listeMotifRead.add(tr5);

        ArrayList<Tuple> listeMotifWrite = new ArrayList<>();
        Tuple tw1 = new Tuple("foo", "foo");
        Tuple tw2 = new Tuple("foo", 1);
        Tuple tw3 = new Tuple(1, 2);
        Tuple tw4 = new Tuple(3, 5);
        Tuple tw5 = new Tuple(1, "foo");
        listeMotifWrite.add(tw1);
        listeMotifWrite.add(tw2);
        listeMotifWrite.add(tw3);
        listeMotifWrite.add(tw4);
        listeMotifWrite.add(tw5);

        for (Tuple tuple : listeMotifRead){
            final Tuple motif = tuple;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Tuple res = linda.read(motif);
                    System.out.println("(1) Resultat:" + res);

                    linda.debug("(1)" + motif.toString());

                }
            }.start();
        }

        for (Tuple tuple : listeMotifWrite){
            final Tuple motif = tuple;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("(2) write: " + motif);
                    linda.write(motif);

                    linda.debug("(2)" + motif.toString());

                }
            }.start();
        }
    }
}
