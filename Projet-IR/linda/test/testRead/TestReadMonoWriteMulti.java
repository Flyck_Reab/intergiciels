package linda.test.testRead;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;

/**
 * Test :
 * 5 read sur un seul thread
 * 5 write sur cinq thread différent
 */
public class TestReadMonoWriteMulti {


    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");

        ArrayList<Tuple> listeMotifWrite = new ArrayList<>();
        Tuple t1 = new Tuple("foo", "foo");
        Tuple t2 = new Tuple("foo", 1);
        Tuple t3 = new Tuple(1, 2);
        Tuple t4 = new Tuple(3, 5);
        Tuple t5 = new Tuple(1, "foo");
        listeMotifWrite.add(t1);
        listeMotifWrite.add(t2);
        listeMotifWrite.add(t3);
        listeMotifWrite.add(t4);
        listeMotifWrite.add(t5);

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // read (Integer, String) -> (1, "foo")
                Tuple motif1 = new Tuple(Integer.class, String.class);
                Tuple res1 = linda.read(motif1);
                System.out.println("(1) Resultat:" + res1);

                // read (Integer, Integer) -> (3, 5)
                Tuple motif2 = new Tuple(Integer.class, Integer.class);
                Tuple res2 = linda.read(motif2);
                System.out.println("(1) Resultat:" + res2);

                // read (String, String) -> ("foo", "foo")
                Tuple motif3 = new Tuple(String.class, String.class);
                Tuple res3 = linda.read(motif3);
                System.out.println("(1) Resultat:" + res3);

                // take (Integer, Integer) -> (3, 5)
                Tuple motif4 = new Tuple(Integer.class, Integer.class);
                Tuple res4 = linda.take(motif4);
                System.out.println("(1) Resultat:" + res4);

                // read (Integer, Integer) -> (1, 2)
                Tuple motif5 = new Tuple(Integer.class, Integer.class);
                Tuple res5 = linda.read(motif5);
                System.out.println("(1) Resultat:" + res5);

                linda.debug("(1)");
            }
        }.start();

        for (Tuple tuple : listeMotifWrite){
            final Tuple motif = tuple;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("(2) write: " + motif);
                    linda.write(motif);

                    linda.debug("(2)" + motif.toString());

                }
            }.start();
        }
    }
}
