package linda.test.testTake;

import linda.Linda;
import linda.Tuple;

/**
 * Test :
 * 5 write sur un seul thread
 * 5 read sur cinq thread différent
 *      Le premier read est bloquant tant que le dernier write n'est pas fait,
 *      mais il ne bloque pas les autre read/take des autres threads
 */
public class TestTakeMultiWriteMono {


    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");

        // 1
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(String.class, String.class);
                Tuple res = linda.read(motif);
                System.out.println("(1) Read Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 2
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 3
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(String.class, String.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 4
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, Integer.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 5
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, Integer.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple("foo", "foo");
                System.out.println("(2) write: " + t1);
                linda.write(t1);

                Tuple t2 = new Tuple("foo", 1);
                System.out.println("(2) write: " + t2);
                linda.write(t2);

                Tuple t3 = new Tuple(1, 2);
                System.out.println("(2) write: " + t3);
                linda.write(t3);

                Tuple t4 = new Tuple(3, 5);
                System.out.println("(2) write: " + t4);
                linda.write(t4);

                Tuple t5 = new Tuple(1, "foo");
                System.out.println("(2) write: " + t5);
                linda.write(t5);

                Tuple t6 = new Tuple("foo2", "foo2");
                System.out.println("(2) write: " + t6);
                linda.write(t6);

                linda.debug("(2)");

            }
        }.start();
    }
}
