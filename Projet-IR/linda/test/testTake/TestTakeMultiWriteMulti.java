package linda.test.testTake;

import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;

/**
 * Test :
 * 5 write sur cinq thread différents
 * 5 read/Take sur cinq thread différents
 */
public class TestTakeMultiWriteMulti {


    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");

        ArrayList<Tuple> listeMotifWrite = new ArrayList<>();
        Tuple t1 = new Tuple("foo", "foo");
        Tuple t2 = new Tuple("foo", 1);
        Tuple t3 = new Tuple(1, 2);
        Tuple t4 = new Tuple(3, 5);
        Tuple t5 = new Tuple(1, "foo");
        Tuple t6 = new Tuple("foo2", "foo2");
        listeMotifWrite.add(t1);
        listeMotifWrite.add(t2);
        listeMotifWrite.add(t3);
        listeMotifWrite.add(t4);
        listeMotifWrite.add(t5);
        listeMotifWrite.add(t6);

        // 1
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(String.class, String.class);
                Tuple res = linda.read(motif);
                System.out.println("(1) Read Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 2
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 3
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(String.class, String.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 4
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, Integer.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        // 5
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, Integer.class);
                Tuple res = linda.take(motif);
                System.out.println("(1) Take Resultat:" + res);

                linda.debug("(1)" + motif.toString());
            }
        }.start();

        for (Tuple tuple : listeMotifWrite){
            final Tuple motif = tuple;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("(2) write: " + motif);
                    linda.write(motif);

                    linda.debug("(2)" + motif.toString());

                }
            }.start();
        }
    }
}
