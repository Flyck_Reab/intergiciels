package linda.test.testTake;

import linda.Linda;
import linda.Tuple;

/**
 * Test :
 * 5 write sur un seul thread
 * 5 read/take sur un seul thread
 */

public class TestTakeMonoWriteMono {

    public static void main(String[] a) {

//        final Linda linda = new linda.shm.CentralizedLinda();
        final Linda linda = new linda.server.LindaClient("rmi://localhost:1099/MonLinda");
        System.out.println("Connected to server");

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // read (Integer, String) -> ("foo", "foo")
                Tuple motif1 = new Tuple(String.class, String.class);
                Tuple res1 = linda.read(motif1);
                System.out.println("(1) Resultat:" + res1);

                // read (Integer, Integer) -> (1, 2)
                Tuple motif2 = new Tuple(Integer.class, Integer.class);
                Tuple res2 = linda.take(motif2);
                System.out.println("(1) Resultat:" + res2);

                // read (String, String) -> ("foo", "foo")
                Tuple motif3 = new Tuple(String.class, String.class);
                Tuple res3 = linda.take(motif3);
                System.out.println("(1) Resultat:" + res3);

                // take (Integer, Integer) -> (3, 5)
                Tuple motif4 = new Tuple(Integer.class, Integer.class);
                Tuple res4 = linda.take(motif4);
                System.out.println("(1) Resultat:" + res4);

                // read (Integer, Integer) -> (foo, 1)
                Tuple motif5 = new Tuple(String.class, Integer.class);
                Tuple res5 = linda.take(motif5);
                System.out.println("(1) Resultat:" + res5);

                linda.debug("(1)");
            }
        }.start();

        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple("foo", "foo");
                System.out.println("(2) write: " + t1);
                linda.write(t1);

                Tuple t2 = new Tuple("foo", 1);
                System.out.println("(2) write: " + t2);
                linda.write(t2);

                Tuple t3 = new Tuple(1, 2);
                System.out.println("(2) write: " + t3);
                linda.write(t3);

                Tuple t4 = new Tuple(3, 5);
                System.out.println("(2) write: " + t4);
                linda.write(t4);

                Tuple t5 = new Tuple(1, "foo");
                System.out.println("(2) write: " + t5);
                linda.write(t5);
                                
                linda.debug("(2)");

            }
        }.start();
    }
}
